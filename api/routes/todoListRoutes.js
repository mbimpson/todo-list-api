import todoListController from '../controllers/todoListController';

export function registerRoutes (app) {
    app.route('/tasks')
    .get(todoListController.list_all_tasks)
    .post(todoListController.create_a_task);


    app.route('/tasks/:taskId')
    .get(todoListController.read_a_task)
    .put(todoListController.update_a_task)
    .delete(todoListController.delete_a_task);
}
