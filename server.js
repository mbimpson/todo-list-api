import express from 'express';
const app = express();
const port = process.env.PORT || 3000;
import model from './api/models/todoListModel';
import { registerRoutes } from './api/routes/todoListRoutes';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/Tododb');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

registerRoutes(app);

app.use(function(req, res) {
    res.status(404).send({url: req.originalUrl + ' not found'})
});

app.listen(port, () => console.log('api listening on port: ' + port));

export default app;
